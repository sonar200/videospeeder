let ctsa = {
    settings: {
        enable: false,
        speed: 2,
        defaultSpeed: 1
    },
    actions: {
        toggle: (data) => {
            ctsa.settings.enable = data;
            chrome.storage.local.set(ctsa.settings);
            ctsa.badgeEnable();

            chrome.tabs.query({}, function (tabs) {
                for (const indexTab in tabs) {
                    chrome.tabs.sendMessage(
                        tabs[indexTab].id,
                        {type: 'ctsa-update', data: ctsa.settings}
                    );
                }
            });

        },
        setSpeed: (data) => {
            ctsa.settings.speed = data > 0 ? (data < 10 ? data : 10) : ctsa.settings.defaultSpeed;
            chrome.storage.local.set(ctsa.settings);
            ctsa.badgeEnable();

            chrome.tabs.query({}, function (tabs) {
                for (const indexTab in tabs) {
                    chrome.tabs.sendMessage(
                        tabs[indexTab].id,
                        {type: 'ctsa-update', data: ctsa.settings}
                    );
                }
            });
        },
        getStatus: (data, callbackFunction) => {
            callbackFunction(ctsa.settings)
        }
    },
    badgeEnable: () => {
        chrome.browserAction.setBadgeBackgroundColor(
            {color: ctsa.settings.enable ? '#4c9100' : '#ab2d2d'});
        chrome.browserAction.setBadgeText({
            text: ctsa.settings.enable ? ctsa.settings.speed.toString()
                : ctsa.settings.defaultSpeed.toString()
        });
    }
}

chrome.storage.local.get(settings => {
    for (const key in ctsa.settings) {
        if (settings.hasOwnProperty(key)) {
            ctsa.settings[key] = settings[key];
        } else {
            chrome.storage.local.set({key: ctsa.settings[key]});
        }
    }
    ctsa.badgeEnable();
})

chrome.runtime.onMessage.addListener(function (message, sender, callbackFunction) {
    if (message.type) {
        const type = message.type.split('-');
        if (type[0] === 'ctsa') {
            if (ctsa.actions.hasOwnProperty(type[1])) {
                ctsa.actions[type[1]](message.data, callbackFunction);
            }
        }
    }

});