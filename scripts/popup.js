'use strict';
const ctsa = {settings: {}}

let number = (value) => {
    return value ? parseFloat(value
        .toString()
        .replace(/\s+/g, '')
        .replace(',', '.')
        .toString()) : 0.0;
}

chrome.runtime.sendMessage({type: 'ctsa-getStatus', data: {}}, function (settings) {
    ctsa.settings = settings;
    document.querySelector('input[name="enable"]').checked = ctsa.settings.enable;
    document.querySelector('input[name="speed"]').value = ctsa.settings.speed;
});

document.querySelector('input[name="enable"]').addEventListener('change', event => {
    chrome.runtime.sendMessage({type: 'ctsa-toggle', data: event.target.checked});
})
document.querySelector('input[name="speed"]').addEventListener('change', event => {
    let speed = number(event.target.value);

    if(speed <= 0){
        speed = 0.1;
    } else if(speed > 10){
        speed = 10;
    }
    event.target.value = speed;
    chrome.runtime.sendMessage({type: 'ctsa-setSpeed', data: speed});
})

// enablePlayerElement.on('change', function (event) {
//     chrome.runtime.sendMessage({type: 'enablePlayer', data: {status:
// event.currentTarget.checked}}); });
