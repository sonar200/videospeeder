const ctsa = {
    settings: {},
    actions: {
        update: (settings) => {
            ctsa.settings = settings;

            if(ctsa.listNodes.length > 0){
                for(const node of ctsa.listNodes){
                    ctsa.setPlayback(node);
                }
            }
            return true;
        }
    },
    listNodes: [],

    setNode: node => {
        if(ctsa.listNodes.length > 0){
            let hasNode = false;
            for (const nodeKey of ctsa.listNodes) {
                if(nodeKey.isEqualNode(node)){
                    hasNode = true;
                    break;
                }
            }

            if(!hasNode){
                ctsa.listNodes.push(node);
            }
        } else {
            ctsa.listNodes.push(node);
        }
    },

    timeout: undefined,

    setPlayback: node => {
        if(ctsa.settings.enable){
            let speed = Number(ctsa.number(ctsa.settings.speed).toFixed(2));
                speed = speed > 0 ? speed : ctsa.settings.defaultSpeed;
                ctsa.showSpeed(speed);
            node.playbackRate = speed > 0 ? speed : ctsa.settings.defaultSpeed;
        } else {
            // ctsa.showSpeed(ctsa.settings.defaultSpeed);
            node.playbackRate = ctsa.settings.defaultSpeed;
        }
    },

    showSpeed: speed => {
        let div = document.querySelector('.f-speed-info');

        if(!div){
            div = document.createElement('div');
            div.classList.add('f-speed-info');
            div.style.display = 'flex';
            div.style.justifyContent = 'center';
            div.style.alignItems = 'center';
            div.style.color = '#FFFF00';
            div.style.width = '50px';
            div.style.height = "50px";
            div.style.position = "fixed";
            div.style.fontSize = "20px";
            div.style.top = '0';
            div.style.left = '0';
            div.style.backgroundColor = "#00000099";
            div.style.zIndex = '1000000';
            document.body.appendChild(div);
        }
        div.innerText = speed;

        clearTimeout(ctsa.timeout)
        ctsa.timeout = setTimeout(() => {
            div.outerHTML = '';
        }, 5000)
    },

    number: (value) => {
        return value ? parseFloat(value
            .toString()
            .replace(/\s+/g, '')
            .replace(',', '.')
            .toString()) : 0.0;
    },

    /**
     *
     * @param {KeyboardEvent} event
     */
    eventHotkey: event => {
        const code = event?.code?.toLowerCase();

        if(!code){
            return false;
        }

        switch (code) {
            case "keyd":
                let speedUp = ctsa.number(ctsa.settings.speed);
                speedUp = (speedUp + 0.1).toFixed(2);


                chrome.runtime.sendMessage({type: 'ctsa-setSpeed', data: speedUp});
                break;
            case "keys":
                let speedDown = ctsa.number(ctsa.settings.speed);
                speedDown = (speedDown - 0.1).toFixed(2);

                chrome.runtime.sendMessage({type: 'ctsa-setSpeed', data: speedDown});
                break;
            case "keyr":
                chrome.runtime.sendMessage({type: 'ctsa-setSpeed', data: ctsa.settings.defaultSpeed});
                break;
        }
    }
}

chrome.runtime.sendMessage({type: 'ctsa-getStatus', data: {}}, function (settings) {
    ctsa.settings = settings;
});

chrome.runtime.onMessage.addListener(function (message, sender, callbackFunction) {
    const type = message.type.split('-');
    if (type[0] === 'ctsa') {
        if (message.type && ctsa.actions.hasOwnProperty(type[1])) {
            ctsa.actions[type[1]](message.data, callbackFunction);
        }
    }
});

let observer = new MutationObserver(mutations => {
    for (let mutation of mutations) {
        if (mutation.addedNodes.length > 0) {
            for (let node of mutation.addedNodes) {
                if (node instanceof HTMLElement) {
                    if (node.tagName.toLowerCase() === 'video') {
                        ctsa.setNode(node);
                        ctsa.setPlayback(node);
                    }
                }
            }
        } else if (mutation.target.tagName.toLowerCase() === 'video') {
            ctsa.setNode(mutation.target);
            ctsa.setPlayback(mutation.target);
        }
    }
});

observer.observe(window.document, {childList: true, subtree: true, attributes: true});

document.addEventListener("DOMContentLoaded", () => {
    document.addEventListener('keyup', ctsa.eventHotkey)
})